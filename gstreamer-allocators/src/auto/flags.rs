// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use bitflags::bitflags;
use glib::translate::*;

bitflags! {
    pub struct FdMemoryFlags: u32 {
        const NONE = 0;
        const KEEP_MAPPED = 1;
        const MAP_PRIVATE = 2;
        const DONT_CLOSE = 4;
    }
}

#[doc(hidden)]
impl ToGlib for FdMemoryFlags {
    type GlibType = ffi::GstFdMemoryFlags;

    fn to_glib(&self) -> ffi::GstFdMemoryFlags {
        self.bits()
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstFdMemoryFlags> for FdMemoryFlags {
    unsafe fn from_glib(value: ffi::GstFdMemoryFlags) -> FdMemoryFlags {
        skip_assert_initialized!();
        FdMemoryFlags::from_bits_truncate(value)
    }
}

